import './App.css';
import Navbar from "./components/navbar/navbar";
import MainSection from "./components/MainSection";
import FiguresSection from "./components/FiguresSection";
import RedSection from "./components/RedSection";
import Implements from "./components/Implements";
import Staffs from "./components/Staffs";
import RequesSection from "./components/RequesSection";
import Footer from "./components/Footer";

function App() {
  return (
      <div>
        <Navbar/>
        <MainSection/>
        <FiguresSection/>
        <RedSection/>
        <Implements/>
        <Staffs/>
        <RequesSection/>
        <Footer/>
      </div>
  );
}

export default App;
