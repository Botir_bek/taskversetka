import React from 'react';
import styled from "styled-components";
import Container from "./Container";
import Background from '../assets/images/background_light.png'
import {P2, Title} from "./MainSectionOptions";
import Staff5 from '../assets/images/staff_1.svg'
import Staff4 from '../assets/images/staff_2.svg'
import Staff3 from '../assets/images/staff_3.svg'
import Staff2 from '../assets/images/staff_4.svg'
import Staff1 from '../assets/images/staff_5.svg'
import {Arrows} from "./FiguresSection";
import Forums from "./Forums";

const Staffs = () => {

    const data = [
        {
            img:Staff1,
            name:'Раймм Лидия Вячеславовна',
            desc:'Генеральный директор'
        },
        {
            img:Staff2,
            name:'Золотов Геннадий Борисович',
            desc:'Руководитель отдела патентно-юридического делопроизводства'
        },
        {
            img:Staff3,
            name:'Осоргина Екатерина Александровна',
            desc:'Патентный поверенный РФ'
        },
        {
            img:Staff4,
            name:'Рачеева Юлия Геннадьевна',
            desc:'Патентный поверенный РФ'
        },
        {
            img:Staff5,
            name:'Пахоменко Алина Аликовна',
            desc:'Адвокат'
        },
        {
            img:Staff4,
            name:'Рачеева Юлия Геннадьевна',
            desc:'Патентный поверенный РФ'
        },
    ]
    return (
        <Wrapper>
            <Container>
                <div>
                    <Title style={{color:'#000'}}>Команда экспертов</Title>
                    <P2 style={{color:'#8A8A8A'}}>Наша команда — эксперты в сфере IP. Мы берёмся за работу и доводим дело до конца, потому что понимаем ценности вашего бизнеса</P2>
                    <StaffsWrapper>
                        { data.map((item,index)=>(
                            <div key={index}>
                                <img src={item.img} alt={'img'}/>
                                <p>{item.name}</p>
                                <span>{item.desc}</span>
                            </div>
                        ))}
                    </StaffsWrapper>
                    <Arrows>
                        <div><span style={{left:-22}}>←</span></div>
                        <div><span style={{right:-22}}>→</span></div>
                    </Arrows>
                    <Forums/>
                </div>
            </Container>
        </Wrapper>
    );
};

export default Staffs;

const Wrapper = styled.div`
  padding: 160px 0px;
  background: url(${Background});
  background-size: 100% 100%;
`

const StaffsWrapper = styled.div`
  display: flex;
  gap: 20px;
  overflow: hidden;
  flex-direction: row;
  //width: 2000px;
  margin-top: 50px;

  div {
    display: flex;
    flex-direction: column;
    width: 289px;

    img {
      border-radius: 8px;
      filter: grayscale(100%);
    }

    p {
      font-style: normal;
      font-weight: 900;
      font-size: 18px;
      line-height: 160%;
      color:  #1B1B1B;
      width: 200px;
      margin: 10px 0px 5px;
    }
    span{
      font-family: 'Montserrat';
      font-style: normal;
      font-weight: 400;
      font-size: 14px;
      line-height: 140%;
      display: flex;
      align-items: center;
      color: #5C5C5C
    }
  }
`;
