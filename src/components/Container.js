import React from 'react';
import styled from "styled-components";

const Container = (props) => {
    return (
        <Wrapper>
            {props.children}
        </Wrapper>
    );
};

export default Container;

const Wrapper = styled.div`
  width: 1400px;
  margin: auto;
`;
