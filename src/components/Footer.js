import React from 'react';
import Container from "./Container";
import styled from "styled-components";
import {ReactComponent as Logo} from "../assets/icons/large-logo.svg";

const Footer = () => {
    return (
        <Wrapper>
            <Container>
                <div>
                    <Top>
                        <div>
                            <Logo/>
                            <p>Общество с ограниченной
                                ответственностью «Центр интеллектуальной собственности и инноваций „АйПиХаб“»</p>
                        </div>
                        <div>
                            <p>
                                123317, г. Москва, Пресненская наб., д.
                                8, стр. 1, эт. 3, п. 1, оф. 10
                            </p>
                            <p>
                                <span>ИНН:</span> 7703472433
                                <span>КПП:</span> 770301001
                            </p>
                        </div>
                    </Top>
                    <Line></Line>
                    <Lists>
                        <div>
                            <p>Навигация по сайту:</p>
                            <p>Услуги</p>
                            <p>Кейсы</p>
                            <p>FAQ</p>
                        </div>
                        <div>
                            <p>Наши контакты:</p>
                            <p>example@iphub.com</p>
                            <p>+7 (888) 888 88 88</p>
                        </div>
                        <div>
                            <p>Мы в соцсетях:</p>
                            <p>Facebook</p>
                            <p>Instagram</p>
                        </div>
                        <div>
                            <p>Как добраться:</p>
                            <p>Схема проезда</p>
                        </div>
                    </Lists>
                    <Bottom>
                        <p>© 2021 IPhub. Все права защищены.</p>
                        <p>Публичная оферта</p>
                        <p>Пользовательское соглашение</p>
                    </Bottom>
                </div>
            </Container>
        </Wrapper>
    );
};

export default Footer;

const Wrapper = styled.div`
  background: rgba(0,0,0,0.9);
  padding: 100px 0px 50px;
`;

const Top = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  div{
    &:nth-child(1){
      display: flex;
      align-items: center;
      gap: 20px;
      svg{
        width: 209px;
      }
      p{
        font-family: 'Montserrat';
        font-style: normal;
        font-weight: 400;
        font-size: 14px;
        line-height: 165%;
        color: #8A8A8A;
        width: 515px;
      }
    }
    &:nth-child(2){
      p{font-family: 'Montserrat';
        font-style: normal;
        font-weight: 400;
        font-size: 14px;
        line-height: 165%;
        color: #FFFFFF;
        width:289px;
        &:nth-child(2){
          span{
            color: #8A8A8A;
            &:nth-child(2){
              margin-left: 15px;
            }
          }
        }
      }
    }
  }
`;

const Line = styled.div`
  width: 100%;
  height: 1px;
  background: rgb(92,92,92);
  background: linear-gradient(90deg, rgba(92,92,92,1) 48%, rgba(22,22,22,1) 100%);
  margin:50px 0px 66px;
`;

const Lists = styled.div`
  display: flex;
  justify-content: space-between;
  div{
    p{
      font-family: 'Montserrat';
      font-style: normal;
      font-weight: 500;
      font-size: 16px;
      line-height: 160%;
      color: #FFFFFF;
      margin:15px 0px 0px;
      &:nth-child(1){
        color:#5C5C5C;
        font-size: 18px;
        margin: 0px 0px 20px 2px;
      }
    }
  }
`;

const Bottom = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-top: 50px;
  p{
    font-family: 'Montserrat';
    font-style: normal;
    font-weight: 400;
    font-size: 14px;
    line-height: 165%;
    color: #FFFFFF;
    &:nth-child(1){
      color: #bababa;
    }
  }
`;
