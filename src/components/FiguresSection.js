import React from 'react';
import Container from "./Container";
import Background from '../assets/images/backgroud_grey.svg'
import styled from "styled-components";
import {P2, Title} from "./MainSectionOptions";
import Shape from '../assets/images/shape_1.svg'
import Shape2 from '../assets/images/shape_2.svg'
import Shape3 from '../assets/images/shape_3.svg'
import Shape4 from '../assets/images/shape_4.svg'
import Shape5 from '../assets/images/shape_5.svg'
import {ReactComponent as Arrow} from "../assets/icons/arrow.svg";


const FiguresSection = () => {

    const data = [
        {
            img:Shape,
            description:'Регистрация товарного знака в Роспатенте',
            dscrpt:'Изобретения и Полезные модели '
        },{
            img:Shape2,
            description:'Регистрация прав инновационные устройства, способы или технологии',
            dscrpt:'Изобретения и Полезные модели '
        },{
            img:Shape3,
            description:'Промышленные образцы',
            dscrpt:'Изобретения и Полезные модели '
        },{
            img:Shape4,
            description:'Программы для ЭВМ и Базы данных',
            dscrpt:'Изобретения и Полезные модели '
        },{
            img:Shape5,
            description:'Представление интересов, суды',
            dscrpt:'Изобретения и Полезные модели '
        },{
            img:Shape,
            description:'Регистрация товарного знака в Роспатенте',
            dscrpt:'Изобретения и Полезные модели '
        },
    ]

    return (
        <Wrapper>
            <Container>
                <div>
                    <Title style={{paddingTop:160,color:'#000'}}>Решения для вашего бизнеса</Title>
                    <div style={{display:'flex',gap:180,alignItems:'center'}}>
                        <P2 style={{color:'#8A8A8A'}}>У нас есть все необходимые ресурсы для решения любых ваших потребностей в сфере интеллектуальной собственности</P2>
                        <Circle2>Все услуги <span>→</span></Circle2>
                    </div>
                    <CardWrapper>
                        {
                            data.map((item,index)=>(
                                <Card key={index}>
                                    <ArrowIcon><Arrow/></ArrowIcon>
                                    <img src={item.img} alt={'img'}/>
                                    <p>{item.description}</p>
                                </Card>
                            ))
                        }
                    </CardWrapper>
                    <Arrows>
                        <div><span style={{left:-22}}>←</span></div>
                        <div><span style={{right:-22}}>→</span></div>
                    </Arrows>
                </div>
            </Container>
        </Wrapper>
    );
};

export default FiguresSection;


const Wrapper = styled.div`
  background: url(${Background}), #FFFFFF;
  mix-blend-mode: normal;
  padding-bottom: 165px;

`;
const Circle2 =styled.div`
  width: 135px;
  height: 41px;
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  font-family: 'Montserrat';
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 160%;
  /* or 26px */

  display: flex;
  align-items: center;
  text-align: center;

  /* red */
  border: 1px solid #DA4533;
  display: flex;
  align-items: center;
  justify-content: center;
  color: #DA4533;
  position: relative;
  span{
    font-size: 40px;
    position: absolute;
    right: -25px;
  }
`;

const CardWrapper = styled.div`
  display: flex;
  overflow-x: hidden;
  flex-direction: row;
  gap:20px;
  margin-top: 50px;
  //width: 2000px;
`;

const Card = styled.div`
  background: #1B1B1B;
  border-radius: 8px;
  width: 289px;
  height: 400px;
  position: relative;
  img{
    margin:80px 80px 0px 80px;
  }
  p{
    color: #8A8A8A;
    font-style: normal;
    font-weight: 500;
    font-size: 18px;
    line-height: 160%;
    position: absolute;
    left: 25px;
    bottom: 0px;
  }
`;

const ArrowIcon = styled.span`
  position: absolute;
  right:20px;
  top:20px;
  svg{
    fill:#5C5C5C;
  }
`;

export const Arrows = styled.div`
  display: flex;
  gap:20px;
  margin-top: 35px;
  margin-left: 500px;
  div{
    width: 40px;
    height: 40px;
    border-radius: 50%;
    color:#DA4533;
    border:1px solid #DA4533;
    position: relative;
    
    span{
      font-size: 40px;
      position: absolute;
      top:-8px;
    }
  }
`;

