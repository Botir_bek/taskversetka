import React from 'react';
import styled from "styled-components";
import Container from "./Container";
import Background from '../assets/images/backgroud_red.svg'
import Img3 from '../assets/images/img3.png'
import {P2, Title} from "./MainSectionOptions";

const RequesSection = () => {
    return (
        <Wrapper>
            <Container>
                <Box>
                    <Texts>
                        <Title>Оставьте заявку на консультацию и мы вам поможем! </Title>
                        <P2>У нас есть решения для всех ваших потребностей в сфере защиты и регистрации интеллектуальной собственности</P2>
                        <div>Получить индивидуальную консультацию</div>
                    </Texts>
                    <div><img src={Img3} alt={'img'}/></div>
                </Box>
            </Container>
        </Wrapper>
    );
};

export default RequesSection;

const Wrapper =styled.div`
  padding:10px 0px 70px 0px;
  background: url(${Background});
`;

const Box = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
const Texts = styled.div`
  width: 706px;
  h4{
    font-style: normal;
    font-weight: 400;
    font-size: 38px;
    line-height: 135%;
    display: flex;
    align-items: center;
    color: #FFFFFF
  }
  p{
    color: #fff;
    margin: 16px 0px 44px;
  }
  
  div{
    width: 299px;
    height: 91px;
    border-radius: 50%;
    background: #1B1B1B;
    display: flex;
    justify-content: center;
    align-items: center;
    color: #fff;
    text-align: center;
  }
  
`;
