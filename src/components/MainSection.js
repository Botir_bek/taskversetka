import React from 'react';
import Container from "./Container";
import {ReactComponent as MainImg} from "../assets/images/main_img.svg";
import styled from "styled-components";
import MainSectionOptions from "./MainSectionOptions";
import {ReactComponent as RedDot} from "../assets/icons/red1.svg";
import {ReactComponent as RedDot2} from "../assets/icons/red2.svg";

const MainSection = () => {
    return (
        <div style={{background:'rgba(0,0,0,0.9)',paddingTop:70}}>
            <Container>
                <Wrapper>
                    <Wrapper2 style={{width:715}}>
                        <P1>ВАШ IP ПАРТНЕР</P1>
                        <H4>
                            <RedDot/>
                            Интеллектуальная собственность
                            <RedDot2/>
                        </H4>
                        <P2>Индивидуальные правовые решения для защиты и охраны ваших нематериальных активов в России и за рубежом</P2>
                        <div style={{display:'flex',alignItems:'center',gap:42,marginTop:32}}>
                            <Circle1>Получить индивидуальную <br/> консультацию</Circle1>
                            <Circle2>Все услуги <span>→</span></Circle2>
                        </div>
                    </Wrapper2>
                    <div><MainImg/></div>
                </Wrapper>
                <MainSectionOptions/>
            </Container>
        </div>
    );
};

export default MainSection;

const Wrapper = styled.div`
  display: flex;
  align-items: center;
`;

const Wrapper2 = styled.div`
  display: flex;
  flex-direction: column;
  gap:10px;
`;
const P1 =styled.p`
  font-family: 'Montserrat';
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  margin:0px;
  //line-height: 160%;
  /* identical to box height, or 26px */

  display: flex;
  align-items: center;
  text-transform: uppercase;

  /* grey */

  color: #8A8A8A
`;

const H4 = styled.h4`
  font-family: 'STIX Two Text';
  font-style: normal;
  font-weight: 500;
  font-size: 55px;
  margin:0px;
  color: #FFFFFF;
  position: relative;
  svg{
    position: absolute;
    &:nth-child(1){
      top:10px;
      left:-17px;
    }
    &:nth-child(2){
      bottom:40px;
    }
  }
  
`;

const P2 =styled.p`
  font-family: 'Montserrat';
  font-style: normal;
  font-weight: 400;
  font-size: 18px;
  margin:0px;
  line-height: 160%;
  width: 598px;
  display: flex;
  align-items: center;

  /* grey */

  color: #8A8A8A;
`;

const Circle1 = styled.div`
  background: #DA4533;
  width: 299px;
  height: 91px;
  border-radius: 50%;
  font-family: 'Montserrat';
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 160%;
  display: flex;
  justify-content: center;
  align-items: center;
  text-align: center;
  color: #FFFFFF
`;

const Circle2 =styled.div`
  width: 135px;
  height: 41px;
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  font-family: 'Montserrat';
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 160%;
  display: flex;
  align-items: center;
  text-align: center;
  border: 1px solid #DA4533;
  display: flex;
  align-items: center;
  justify-content: center;
  color: #DA4533;
  position: relative;
  span{
    font-size: 40px;
    position: absolute;
    right: -25px;
  }
`;
