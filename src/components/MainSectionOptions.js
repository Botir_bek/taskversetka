import React from 'react';
import Photo from "../assets/images/img2.png";
import styled from "styled-components";
import ReklamaSection from "./ReklamaSection";

const MainSectionOptions = () => {

    const card = [
        {
            title:'>300',
            description:'Представления интересов в спорах по IP правам в год'
        },{
            title:'>2500',
            description:'Представления интересов в спорах по IP правам в год'
        },{
            title:'1150',
            description:'Завершённых дел по регистрации товарных знаков'
        },{
            title:'61',
            description:'Совокупный опыт экспертов'
        },{
            title:'99%',
            description:'Успех получения патентов'
        },{
            title:'98%',
            description:'Успех регистрации товарных знаков'
        },
    ]

    return (
        <Wrapper>
            <div><img src={Photo} alt={'img'}/></div>
            <div>
                <Title>Многолетний опыт</Title>
                <P2>Наша компания была создана в 2019 году специалистами, имеющими опыт работы в сфере интеллектуальной собственности более 20 лет</P2>
                <div style={{
                    marginTop:50,
                    display:'flex',
                    gap:20,
                    flexWrap:'wrap'
                }}>{
                    card.map((item,index)=>(
                        <CardWrapper key={index}>
                            <h2>{item.title}</h2>
                            <p>{item.description}</p>
                        </CardWrapper>
                    ))
                }</div>
                <ReklamaSection/>
            </div>
        </Wrapper>
    );
};

export default MainSectionOptions;

const Wrapper = styled.div`
  display: flex;
  align-items: start;
  gap:48px;
`;

export const Title = styled.h4`
  font-style: normal;
  font-weight: 400;
  font-size: 38px;
  line-height: 135%;
  margin:0px;
  display: flex;
  align-items: center;

  /* white */

  color: #FFFFFF;
`;

export const P2 =styled.p`
  font-family: 'Montserrat';
  font-style: normal;
  font-weight: 400;
  font-size: 18px;
  margin:0px;
  line-height: 160%;
  width: 804px;
  color: #8A8A8A;
`;

const CardWrapper = styled.div`
  display: flex;
  flex-direction: column;
  gap:10px;
  width: 390px;
  height: 137px;
  background: rgba(255, 255, 255, 0.05);
  border-radius: 8px;
  position: relative;
  &:before{
    content: '';
    width: 2px;
    height: 137px;
    position: absolute;
    top:0px;
    left:0px;
    background:  linear-gradient(to bottom, red, rgba(0, 0, 0, 0));
  }
  &:after{
    content: '';
    width: 290px;
    height: 2px;
    position: absolute;
    top:0px;
    left:0px;
    background:  linear-gradient( to right, red, rgba(0, 0, 0, 0));
  }
  
  h2{
    font-style: normal;
    font-weight: 500;
    font-size: 36px;
    line-height: 45px;
    display: flex;
    align-items: center;
    color: #FFFFFF;
    margin: 0px;
    margin: 20px 0px 0px 20px;
  }
  p{
    font-style: normal;
    font-weight: 400;
    font-size: 16px;
    line-height: 160%;
    display: flex;
    align-items: center;
    color: #8A8A8A;
    margin: 0px;
    margin-left:20px;
  }
`;
