import React from 'react';
import {P2, Title} from "./MainSectionOptions";
import Staff1 from '../assets/images/staff_5.svg'
import Staff2 from '../assets/images/staff_4.svg'
import styled from "styled-components";

const Forums = () => {
    return (
        <div>
            <TitleContainer>
                <Title style={{color:'#000'}}>Мы на форумах</Title>
                <div>
                    <P2>Прошедшие</P2>
                    <P2 style={{color:'#DA4533'}}>Будущие</P2>
                </div>
            </TitleContainer>
            <StaffList>
                <StaffContainer>
                    <div><img src={Staff1} alt={'str'}/></div>
                    <div>
                        <P2>Раймм Лидия Вячеславовна</P2>
                        <P2>Генеральный директор IPhub</P2>
                    </div>
                </StaffContainer>
                <MentorTitle>
                    <p>Менторская гостинная</p>
                    <div></div>
                    <p>Индивидуальная 30-ти минутная сессия с разбором вашего кейса.</p>
                </MentorTitle>
                <DateContainer>
                    <P2>9-10 ноября</P2>
                    <P2>18:00</P2>
                </DateContainer>
            </StaffList>
            <LinerGR></LinerGR>
            <StaffList>
                <StaffContainer>
                    <div><img src={Staff2} alt={'str'}/></div>
                    <div>
                        <P2>Золотов Геннадий Борисович</P2>
                        <P2>Руководитель отдела патентно-юридического делопроизводства</P2>
                    </div>
                </StaffContainer>
                <MentorTitle>
                    <p>WIPO SUMMER SCHOOL — 2021 RUSSIA</p>
                    <div></div>
                    <p>Летняя школа Всемирной организации интеллектуальной собственности</p>
                </MentorTitle>
                <DateContainer>
                    <P2>28 июня -<br/> 9 июля</P2>
                    <P2>15:30</P2>
                </DateContainer>
            </StaffList>
        </div>
    );
};

export default Forums;


export const TitleContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  margin: 75px 0px;

  div{
    display: flex;
    gap:66px;
    ont-style: normal;
    font-weight: 500;
    font-size: 16px;
    line-height: 160%;
    p{
      width: auto;
    }
  }
`;

const StaffList = styled.div`
  display: flex;
  justify-content: space-between;
`;

const StaffContainer = styled.div`
  display: flex;
  align-items: center;
  
  div{
    &:nth-child(1){
      width: 100px;
      height: 100px;
      border-radius: 50%;
      margin-right: 20px;
        img{
          width: 100%;
          height: 100%;
          border-radius: 50%;
          object-fit: cover;
          filter: grayscale(100%);

        }
    }
    &:nth-child(2){
      p{
        width: auto;
        &:nth-child(1){
          color:#1B1B1B;
          font-weight: 900;
        }
        &:nth-child(2){
          font-size: 14px;
          width: 330px;
        }
      }
    }
  }
`;

const MentorTitle = styled.div`
  padding-top: 10px;
  width: 495px;
  p{
    font-family: 'Montserrat';
    font-style: normal;
    font-weight: 400;
    font-size: 16px;
    line-height: 160%;
    margin: 0px;
    
    &:nth-child(1){
      color:#1B1B1B;
      font-weight: 900;
      text-align: start;
        font-size: 18px;
    }
    &:nth-child(2){
      font-size: 14px;
      #5C5C5C
    }
  }

  div {
    width: 100%;
    height: 1px;
    margin:5px 0px;
    background: rgb(144,2,2);
    background: linear-gradient(90deg, rgba(144,2,2,1) 0%, #DEDEDE 100%);
  }


`;

const DateContainer = styled.div`
  padding-top: 15px;

  p{
    width: auto;
    
    &:nth-child(1){
      color:#1B1B1B;
      font-weight: 900;

    }
    &:nth-child(2){
      font-size: 14px;
    }
  }
`

const LinerGR = styled.div`
  margin: 60px 0px;
  width: 100%;
  height: 1px;
  background: rgb(92,92,92);
  background: linear-gradient(90deg, rgba(92,92,92,1) 36%, rgba(255,255,255,0) 100%);

`;
