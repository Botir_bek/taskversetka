import React from 'react';
import styled from "styled-components";
import Container from "./Container";
import Background from '../assets/images/backgroud_black.svg'
import {P2, Title} from "./MainSectionOptions";
import Logo1 from '../assets/icons/logo_2_1.svg'
import Logo2 from '../assets/icons/logo_2_2.svg'
import {Arrows} from "./FiguresSection";

const Implements = () => {

    const data = [
        {
            logo:Logo1,
            title:'CIRKLE — производитель косметической продукции',
            description:'Как в результате защиты  мы зарегистрировали бренд российской косметики при наличии оснований для отказа'
        },
        {
            logo:Logo2,
            title:'Camera IQ — производитель систем машинного зрения',
            description:'Защита прав после ребрендинга. Как провести успешную регистрацию'
        },

    ]

    return (
        <Wrapper>
            <Container>
                <div>
                    <Title>Реализованные кейсы</Title>
                    <HeaderTitles>
                        <P2>Наши реальные истории о том, как мы помогали своим клиентам регистрировать товарные знаки и оформлять права на объекты интеллектуальной собственности</P2>
                        <div>Все кейсы<span>→</span></div>
                    </HeaderTitles>
                    <CardWrapper>
                        {data.map((item,index)=>(
                            <CardItem key={index}>
                                <div><img src={item.logo} alt={'img'}/></div>
                                <div>
                                    <P2 style={{color:'#fff',fontSize:18}}>{item.title}</P2>
                                    <P2>{item.description}</P2>
                                </div>
                            </CardItem>
                        ))}
                    </CardWrapper>
                    <Arrows>
                        <div><span style={{left:-22}}>←</span></div>
                        <div><span style={{right:-22}}>→</span></div>
                    </Arrows>
                </div>
            </Container>
        </Wrapper>
    );
};

export default Implements;

const Wrapper = styled.div`
  padding: 160px 0px;
  background:  rgba(0,0,0,0.9);
  backdrop-filter: opacity(60%);
  
`;

const HeaderTitles = styled.div`
  display: flex;
  justify-content: space-between;
  div{
    width: 135px;
    height: 41px;
    border-radius: 50%;
    border:1px solid #DA4533;
    color:#DA4533;
    display: flex;
    justify-content: center;
    align-items: center;
    position: relative;
    span{
      position: absolute;
      right:-23px;
      top:-10px;
      font-size: 40px;
    }
  }
`;


const CardWrapper = styled.div`
  display: flex;
  overflow: hidden;
  flex-direction: row;
  //justify-content: space-between;
  //width: 100%;
  gap:20px;
  margin-top: 50px;
  //width: 1400px;
`;

const CardItem = styled.div`
  width: 598px;
  height: 289px;
  display: flex;
  justify-content: space-between;
  overflow: hidden;
  border-radius: 10px;
  div{
    width: 299px;
    background:#2F2F2F;
    display: flex;
    justify-content: center;
    //flex-direction: column;
    align-items: center;
    &:nth-child(2){
      background: #242424;
      display: flex;
      justify-content: center;
      flex-direction: column;
      p{
        width: 250px;
        font-size: 14px;
      }      
    }
  }
`;

