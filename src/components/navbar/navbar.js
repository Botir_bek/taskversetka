import React from 'react';
import {ReactComponent as Logo} from "../../assets/icons/logo.svg";
import styled from "styled-components";
import Container from "../Container";

const Navbar = () => {
    return (
        <Wrapper>
            <Container>
                <div style={{
                    display:'flex',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                }}>
                <Logo/>
                <p style={{color:'#fff'}}>+7 (888) 888 88 88 <span style={{color:'red',marginLeft:20}}>Получить консультацию</span></p>
                <SubList>
                    <div>Услуги</div>
                    <div>Кейсы</div>
                    <div>Кейсы</div>
                </SubList>
                </div>
            </Container>
        </Wrapper>
    )
}

export default Navbar;

const Wrapper = styled.div`
  background: #1B1B1B;
  position: fixed;
  width: 100%;
  height: 70px;
  display: flex;
  align-items: center;
  //justify-content: stretch;
  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.15);
  z-index: 100;
`;

const SubList = styled.div`
  display: flex;
  gap:30px;
  color:#fff;
`;
