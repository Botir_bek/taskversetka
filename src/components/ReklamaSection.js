import React from 'react';
import {P2, Title} from "./MainSectionOptions";
import  Logo1 from "../assets/icons/logo_1.svg";
import  Logo2 from "../assets/icons/logo_2.svg";
import  Logo3 from "../assets/icons/logo_3.svg";
import  Logo4 from "../assets/icons/logo_4.svg";
import  Logo5 from "../assets/icons/logo_5.svg";
import  Logo6 from "../assets/icons/logo_6.svg";
import styled from "styled-components";

const ReklamaSection = () => {

    const logos = [
        {logo:Logo1},
        {logo:Logo2},
        {logo:Logo3},
        {logo:Logo4},
        {logo:Logo5},
        {logo:Logo6},

    ]

    return (
        <Wrapper>
            <Title>Подтвержденный практикой</Title>
            <P2>Мы и наши клиенты гордимся опытом, который подтверждается нашими результатами от закрепления прав заявителей до успешной защиты интересов</P2>
            <div style={{
                display:'flex',
                gap:20,
                flexWrap:'wrap'
            }}>
                {
                    logos.map((item,index)=>(
                        <Card key={index}><img src={item.logo} alt={'str'}/></Card>
                    ))
                }
            </div>
        </Wrapper>
    );
};

export default ReklamaSection;

const Wrapper = styled.div`
  margin-top: 100px;
  display: flex;
  flex-direction: column;
  gap:16px;
  margin-bottom: 160px;
`;

const Card = styled.div`
  width: 392px;
  height: 119px;
  background: rgba(255, 255, 255, 0.05);
  border-radius: 8px;
  display: flex;
  justify-content: center;
  align-items: center;
`;
