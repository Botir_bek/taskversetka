import React from 'react';
import styled from "styled-components";
import Container from "./Container";
import Background from '../assets/images/backgroud_red.svg'
import {P2, Title} from "./MainSectionOptions";
import Shape1 from '../assets/images/shape_2_1.svg'
import Shape2 from '../assets/images/shape_2_2.svg'
const RedSection = () => {

    const data = [
        {desc:'Регистрацию полезной модели'},
        {desc:'Регистрацию промышленного образца'},
        {desc:'Регистрацию прав на использование полезной модели, промобразца, изобретенния'},
        {desc:'Подготовка заявки на выдачу патента'},
        {desc:'Регистрацию изобретения'},
        {desc:'Подготовка заявки на передачу прав патента'},
    ]
    return (
        <Wrapper>
            <Container>
                <div style={{padding:'160px 0px'}}>
                    <Title>Не знаете, что вам нужно? </Title>
                    <P2 style={{color:'#fff'}}>Пройдите квиз, чтобы выбрать оптимальное решение для Вашей задачи</P2>
                    <SelectsBox>
                        <div style={{
                            display:'flex',
                            justifyContent:'space-between',
                            alignItems:'center'}}>
                            <Title>С какой целью вы обращаетесь?</Title>
                            <Counts>
                                <span style={{color:'red'}}>2</span>/8
                            </Counts>
                        </div>
                        <SelectCards>
                            {data.map((item,index)=>(
                                <div key={index}>{item.desc}</div>
                            ))}
                        </SelectCards>
                        <Actions>
                            <div><span style={{left:-23}}>←</span>Назад</div>
                            <div>Далее<span style={{right:-23}}>→</span></div>
                        </Actions>
                    </SelectsBox>
                    <Title style={{fontSize:24}}>Бонусы после прохождения</Title>
                    <BottomSection>
                        <div>
                            <img src={Shape1} alt={'str'}/>
                            <P2 style={{
                                color:'#fff',
                                fontSize:18
                            }}>Название бонуса в одну или две строки</P2>
                        </div>
                        <div>
                            <img src={Shape2} alt={'str'}/>
                            <P2 style={{
                                color:'#fff',
                                fontSize:18
                            }}>Название бонуса в одну или две строки</P2>
                        </div>
                    </BottomSection>
                </div>
            </Container>
        </Wrapper>
    );
};

export default RedSection;

const Wrapper = styled.div`
  background: url(${Background});
`;

const SelectsBox = styled.div`
  padding: 40px;
  margin-top: 50px;
  background: #1B1B1B;
  border-radius: 10px;
  margin-bottom: 50px;
  width: 1041px;
`;

const Counts = styled.div`
  font-family: 'STIX Two Text';
  font-style: normal;
  font-weight: 500;
  font-size: 36px;
  line-height: 45px;
  color: #fff;
`;

const SelectCards = styled.div`
  display: flex;
  flex-direction: row;
  align-items: start;
  flex-wrap: wrap;
  gap: 15px;
  margin-top: 30px;
  
  div{
    width: 450px;
    padding: 18px 20px;
    background: rgba(255, 255, 255, 0.05);
    font-style: normal;
    font-weight: 500;
    font-size: 16px;
    line-height: 160%;
    color: #F9F9F9;
    &:hover{
      background: #DA4533;
    }
  }
`;

const Actions = styled.div`
  display: flex;
  justify-content: space-between;
  margin-top: 36px;
  
  div{
    width: 135px;
    height: 41px;
    border-radius: 50%;
    border:1px solid #DA4533;
    color:#DA4533;
    display: flex;
    justify-content: center;
    align-items: center;
    position: relative;
    span{
      position: absolute;
      top:-10px;
      font-size: 40px;
    }
  }
`;

const BottomSection = styled.div`
  display: flex;
  gap: 20px;
  margin-top: 25px;
  div{
    width: 392px;
    //height: 131px;
    background: #1B1B1B;
    border-radius: 8px;
    padding: 16px 30px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    
  }
`;
